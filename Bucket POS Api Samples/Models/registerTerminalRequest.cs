﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bucket_POS_Api_Samples.Models
{
    public class registerTerminalRequest
    {
        public string retailerId { get; set; }
        public string terminalId { get; set; }
    }
}
