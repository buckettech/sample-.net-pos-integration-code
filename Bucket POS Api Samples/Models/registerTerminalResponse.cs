﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bucket_POS_Api_Samples.Models
{
    public class registerTerminalResponse
    {
        public string apiKey { get; set; }
        public bool isApproved { get; set; }
    }
}
