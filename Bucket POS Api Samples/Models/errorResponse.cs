﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bucket_POS_Api_Samples.Models
{
    public class errorResponse
    {
        public string errorCode { get; set; }
        public string message { get; set; }
    }
}
