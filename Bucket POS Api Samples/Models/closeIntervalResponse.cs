﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bucket_POS_Api_Samples.Models
{
    public class closeIntervalResponse
    {
        public string intervalId { get; set; }
        public int intervalAmount { get; set; }
        public List<auditTransaction> transactions { get; set; }
    }
}
