﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bucket_POS_Api_Samples.Models
{
    public class createTransactionResponse : createTransactionRequest
    {
        public string customerCode { get; set; }
        public string qrCodeContent { get; set; }
        public string bucketTransactionId { get; set; }
    }
}
