﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bucket_POS_Api_Samples.Models
{
    public class auditTransaction
    {
        public int amount { get; set; }
        public int totalTransactionAmount { get; set; }
        public string intervalId { get; set; }
        public string locationId { get; set; }
        public string clientTransactionId { get; set; }
        public string terminalId { get; set; }
        public string bucketTransactionId { get; set; }
    }
}
