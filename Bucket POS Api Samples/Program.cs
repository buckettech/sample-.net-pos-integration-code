﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net.Http;
using System.Net;
using System.Net.NetworkInformation;
using Bucket_POS_Api_Samples.Models;
using Newtonsoft.Json;
using System.IO;
using System.Diagnostics;
using QRCoder;
using System.Drawing;

namespace Bucket_POS_Api_Samples
{
    class Program
    {
        private static HttpClient BucketClient = new HttpClient();
        static Random rnd = new Random();
        private static string RetailerSecret;
        private static StringBuilder console = new StringBuilder();
        private static string outputFileName = "Bucket_POS_API_Samples_Console_Output.txt";

        [STAThread]
        static async Task Main(string[] args)
        {
            string retailerId = GetConfigValue("BucketRetailerId");
            if(string.IsNullOrWhiteSpace(retailerId))
            {
                WriteToConsole("Program exiting -- no valid retailerId in App.config appSetting BucketRetailerId.");
                WriteToConsole("Press any key.");
                Console.ReadLine();
                return;
            }

            PrepareBucketClient();

            WriteToConsole($"Starting demo -- All console output will be logged to {Directory.GetCurrentDirectory()}\\{outputFileName} for your review.\n");
            WriteToConsole($"Retailer Id used throughout demo is App.config appSetting BucketRetailerId: {retailerId}\n");
            WriteToConsole("Press any key to register this terminal.\n");
            Console.ReadLine();

            // Terminal Registration
            string terminalId = GetFastestMacAddress();

            WriteToConsole($"BEGIN Registering this terminal as \"{terminalId}\". This action is performed on initial terminal setup only.\n");

            string registerTerminalUrl = $"{BucketPosApiBase}{RegisterTerminalPath}";
            var registerRequest = new registerTerminalRequest { terminalId = terminalId, retailerId = retailerId };
            WriteToConsole($"Ready to make POST request to: {registerTerminalUrl} \n");
            WriteToConsole(JsonConvert.SerializeObject(registerRequest, Formatting.Indented));
            WriteToConsole("Press any key to send request.");
            Console.ReadLine();

            var registerTerminalResponse = await GetPostResponse<registerTerminalResponse>(BucketClient, registerTerminalUrl, registerRequest);

            WriteToConsole($"Received Response: {Environment.NewLine}{JsonConvert.SerializeObject(registerTerminalResponse, Formatting.Indented)}{Environment.NewLine}");
            WriteToConsole("Press any key to continue. \n");
            Console.ReadLine();

            WriteToConsole("Note that in Production, registering a terminal is a two step process. At this time, the retailer would go to the Bucket site to approve the terminalId that was just registered.");
            WriteToConsole("SandBox retailers are immediately approved, so here we proceed with fetching the RetailerSecret for this terminal \n");

            // Fetch credentials for approved terminal
            WriteToConsole("BEGIN Getting credentials for this terminal. This action is performed on initial terminal setup only.\n ");
            WriteToConsole($"Ready to make POST request to: {registerTerminalUrl} \n");
            WriteToConsole(JsonConvert.SerializeObject(registerRequest, Formatting.Indented));
            WriteToConsole("Press any key to send request.\n");
            Console.ReadLine(); 

            registerTerminalResponse = await GetPostResponse<registerTerminalResponse>(BucketClient, registerTerminalUrl, registerRequest);
            WriteToConsole($"Received Response: {Environment.NewLine}{JsonConvert.SerializeObject(registerTerminalResponse, Formatting.Indented)}{Environment.NewLine}");
            WriteToConsole("Press any key to continue. \n");
            Console.ReadLine();

            // In production, commit RetailerSecret to a safe local secret store.
            RetailerSecret = registerTerminalResponse.apiKey;

            BucketClient.DefaultRequestHeaders.Add("x-functions-key", RetailerSecret);

            // Create sample transactions
            WriteToConsole("BEGIN Create Transactions. This action is performed for every Bucket transaction");

            // This should be your unique identifier for the business day. 
            string intervalId = Guid.NewGuid().ToString();
            string postTransactionUrl = $"{BucketPosApiBase}{CreateTransactionPath}/{retailerId}";
            int numberOfSamples = 2;
            for (int i = 0; i < numberOfSamples; i++)
            {
                WriteToConsole($"Creating sample transaction {i + 1} of {numberOfSamples}.");
                var newTransactionRequest = GetRandomTransactionRequest(terminalId, intervalId);
                WriteToConsole($"Ready to make POST request to: {postTransactionUrl} \n");
                WriteToConsole(JsonConvert.SerializeObject(newTransactionRequest, Formatting.Indented));
                WriteToConsole("Press any key to send request.\n");
                Console.ReadLine();
                var newTransactionResponse = await GetPostResponse<createTransactionResponse>(BucketClient, postTransactionUrl, newTransactionRequest);
                WriteToConsole($"Received Response: {Environment.NewLine}{JsonConvert.SerializeObject(newTransactionResponse, Formatting.Indented)}{Environment.NewLine}");
                string qrImageLocation = GenerateQRCode(newTransactionResponse.qrCodeContent, $"QR-{newTransactionResponse.bucketTransactionId}");
                WriteToConsole($"QR Bitmap saved as {qrImageLocation}");
                //Process.Start(qrImageLocation);
                WriteToConsole("Press any key to continue.\n");
                Console.ReadLine();
            }

            // Close Interval
            WriteToConsole("BEGIN Close Interval. This action is performed once at the end of every business day.");
            WriteToConsole("Close Interval need only be performed once at the end of every business day to triger an out-of-band financial reconciliation with the retailer.");
            WriteToConsole("This action should be performed by a \"Master\" terminal or by a \"Back Office app\", but subsequent requests for the same intervalId will just be ignored");

            string closeIntervalUrl = $"{BucketPosApiBase}{CloseTerminalPath}/{retailerId}/{intervalId}";

            WriteToConsole($"Ready to make GET request to: {closeIntervalUrl} \n");
            WriteToConsole("Press any key to send request.");
            Console.ReadLine();
            var intervalCloseResponse = await GetGetResponse<closeIntervalResponse>(BucketClient, closeIntervalUrl);
            WriteToConsole($"Received Response: {Environment.NewLine}{JsonConvert.SerializeObject(intervalCloseResponse, Formatting.Indented)}{Environment.NewLine}");
            WriteToConsole("Press any key to continue.\n");
            Console.ReadLine();

            // Attempt transaction on Closed Interval
            WriteToConsole("BEGIN Create Transaction on previously closed interval. This action should never be performed. ");
            WriteToConsole("Provided here to show error. This might be useful to catch in situations where the intervalId is lazy loaded from a shared cache.");
            var erroneousTranRequest = GetRandomTransactionRequest(terminalId, intervalId);
            WriteToConsole($"Ready to make POST request to: {postTransactionUrl} \n");
            WriteToConsole(JsonConvert.SerializeObject(erroneousTranRequest, Formatting.Indented));
            WriteToConsole("Press any key to send request.");
            Console.ReadLine();
            var erroneousTransactionResponse = await GetPostResponse<errorResponse>(BucketClient, postTransactionUrl, erroneousTranRequest);
            WriteToConsole($"Received Response: {Environment.NewLine}{JsonConvert.SerializeObject(erroneousTransactionResponse, Formatting.Indented)}{Environment.NewLine}");
            WriteToConsole("Press any key to continue.\n");
            Console.ReadLine();

            // De-register Terminal
            WriteToConsole("BEGIN Deregister terminal. This action would only be performed at the end-of-life of the terminal or on an explicit Bucket integration uninstall.");
            WriteToConsole("It is performed here to clean up the Bucket Sandbox environment");

            string deregisterTerminalUrl = $"{BucketPosApiBase}{DeregisterTerminalPath}/{retailerId}/{terminalId}";
            WriteToConsole($"Ready to make DELETE request to: {deregisterTerminalUrl} \n");
            WriteToConsole("Press any key to send request.");
            Console.ReadLine();
            await GetDeleteResponse<string>(BucketClient, deregisterTerminalUrl);
            WriteToConsole($"Response on Deregister is 200 - HttpStatusCode.OK {Environment.NewLine}");

            // Dump console to a text file for reference

            File.WriteAllText(outputFileName, console.ToString());
            WriteToConsole($"All Console output written to {outputFileName}. Press any key to exit.\n");
            Console.ReadLine();
            Process.Start("explorer.exe", Directory.GetCurrentDirectory());
        }
        
        private static void WriteToConsole(string text)
        {
            Console.WriteLine(text);
            console.AppendLine(text);
        }

        // Single HttpClient instance used to avoid socket exhaustion.
        // ConnectionLeaseTimeout set to ensure DNS changes are picked up 
        private static void PrepareBucketClient()
        {
            var sp = ServicePointManager.FindServicePoint(new Uri(BucketPosApiBase));
            sp.ConnectionLeaseTimeout = 60000; 
        }
        // In production, be mindful that when using a MAC address in desktop terminal or modular network adapter
        // environments(where the MAC address could be deliberately changed in software or mistakenly by
        // replacing hardware components), Bucket will no longer accept requests from that terminal until the
        // terminal is registered and approved for the specific retailer.
        // Bucket recommends that you use an immutable and unique terminalId instead.
        protected static string GetFastestMacAddress ()
        {
            return NetworkInterface.GetAllNetworkInterfaces()
                .Where(x => x.OperationalStatus == OperationalStatus.Up && x.NetworkInterfaceType != NetworkInterfaceType.Loopback)
                .OrderByDescending(x => x.Speed)
                .Select(x => x.GetPhysicalAddress().ToString())
                .FirstOrDefault();
        }

        public static createTransactionRequest GetRandomTransactionRequest(string terminalId, string intervalId)
        {
            int bucketAmount = rnd.Next(1, 100); 
            createTransactionRequest response = new createTransactionRequest
            {
                amount = bucketAmount,
                totalTransactionAmount = (rnd.Next(1, 100) * 100) - bucketAmount, // random total dollars between 1 and 100
                intervalId = intervalId,
                locationId = Environment.MachineName,
                clientTransactionId = Guid.NewGuid().ToString(),
                terminalId = terminalId
            };

            return response;
        }
        // This method uses a third party library available from NuGet. Any library may be used, but we have found this one to be the most straight-forward.
        protected static string GenerateQRCode(string payload, string fileName)
        {
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qRCodeData = qrGenerator.CreateQrCode(payload, QRCodeGenerator.ECCLevel.Q);

            QRCode qrCode = new QRCode(qRCodeData);
            Bitmap qrImage = qrCode.GetGraphic(20);
            qrImage.Save($"{fileName}.bmp");

            return $"{Directory.GetCurrentDirectory()}\\{fileName}.bmp";
        }

        #region Http helper methods
        public static async Task<T> GetPostResponse<T>(HttpClient client, string url, object body)
        {
            var response = await client.PostAsync(url, new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json"));
            var content = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<T>(content);
        }

        public static async Task<T> GetGetResponse<T>(HttpClient client, string url)
        {
            var response = await client.GetAsync(url);
            var content = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<T>(content);
        }

        public static async Task<T> GetDeleteResponse<T>(HttpClient client, string url)
        {
            var response = await client.DeleteAsync(url);
            var content = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<T>(content);
        }
        #endregion
        /// <summary>
        /// This is NOT a secure implementation for managing secrets. 
        /// In the case of the BucketRetailerSecret, it should be replaced with the implementation dictated by your environment or internal practices
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        protected static string GetConfigValue(string key) => ConfigurationManager.AppSettings[key] ?? string.Empty;

        protected static string RetailerId => GetConfigValue("BucketRetailerId");
        protected static string BucketPosApiBase => "https://sandboxretailerapi.bucketthechange.com/"; // This is the Sandbox base
        protected static string RegisterTerminalPath => "api/registerterminal";
        protected static string DeregisterTerminalPath => "api/deregisterterminal";
        protected static string CreateTransactionPath => "api/transaction";
        protected static string CloseTerminalPath => "api/closeinterval";
        
    }
}
